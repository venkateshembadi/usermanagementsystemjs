function updateUser() {

    var uid = document.getElementById("uid1").value;
    var name = document.getElementById("name1").value;
    var mno = document.getElementById("mno1").value;
    var sapId = document.getElementById("sapId1").value;
    var email = document.getElementById("email1").value;
    var gender = document.getElementById("gender1").nodeValue;
    var gender;
    var ele = document.getElementsByName('gender1');
    for (i = 0; i < ele.length; i++) {
        if (ele[i].checked)
            gender = ele[i].value + " ";
    }
    var obj = { name: name, mno: mno, sapId: sapId, email: email, gender: gender };
    var httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpRequest.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            console.log(this.response);

        }
    }
    var url = "http://localhost:3000/users/" + uid;
    httpRequest.open("put", url, true);
    httpRequest.setRequestHeader("Content-type", "application/json");
    httpRequest.send(JSON.stringify(obj));
}
