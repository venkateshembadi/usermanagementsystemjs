function fetchUser() {
    var httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");//old browers will not support AJAX calls
    }
    httpRequest.onreadystatechange = function () {
        if (this.readyState === 4 && this.status == 200) {
            var table = document.createElement("table");

            var tbody = document.createElement("tbody");
            var thead = document.createElement("thead");

            var tr = document.createElement("tr");

            var td1 = document.createElement("td");
            var thName1 = document.createTextNode("Name");
            td1.append(thName1);
            td1.style.border = "1px solid black";

            var td2 = document.createElement("td");
            var thMNo = document.createTextNode("MobileNumber");
            td2.append(thMNo);
            td2.style.border = "1px solid black";

            var td3 = document.createElement("td");
            var thSapId = document.createTextNode("SapId");
            td3.append(thSapId);
            td3.style.border = "1px solid black";

            var td4 = document.createElement("td");
            var thEmail = document.createTextNode("Email");
            td4.append(thEmail);
            td4.style.border = "1px solid black";


            var td5 = document.createElement("td");
            var thGender = document.createTextNode("Gender");
            td5.append(thGender);
            td5.style.border = "1px solid black";

            var td6 = document.createElement("td");
            var thId = document.createTextNode("Id");
            td6.append(thId);
            td6.style.border = "1px solid black";

            table.style.border = "1px solid black";

            tr.append(td1);
            tr.append(td2);
            tr.append(td3);
            tr.append(td4);
            tr.append(td5);
            tr.append(td6);

            thead.appendChild(tr);
            table.appendChild(thead);
            table.appendChild(tbody);


            var data = JSON.parse(this.response);
            var length = data.length;
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    var tableBody = document.createElement("tr");

                    var td1Body = document.createElement("td");
                    var textNode1 = document.createTextNode(data[i].name);
                    td1Body.append(textNode1);

                    var td2Body = document.createElement("td");
                    var textNode2 = document.createTextNode(data[i].mno);
                    td2Body.append(textNode2);

                    var td3Body = document.createElement("td");
                    var textNode3 = document.createTextNode(data[i].sapId);
                    td3Body.append(textNode3);

                    var td4Body = document.createElement("td");
                    var textNode4 = document.createTextNode(data[i].email);
                    td4Body.append(textNode4);

                    var td5Body = document.createElement("td");
                    var textNode5 = document.createTextNode(data[i].gender);
                    td5Body.append(textNode5);

                    var td6Body = document.createElement("td");
                    var textNode6 = document.createTextNode(data[i].id);
                    td6Body.append(textNode6);

                    td1Body.style.border = "1px solid black";
                    td2Body.style.border = "1px solid black";
                    td3Body.style.border = "1px solid black";
                    td4Body.style.border = "1px solid black";
                    td5Body.style.border = "1px solid black";
                    td6Body.style.border = "1px solid black";

                    tableBody.append(td1Body);
                    tableBody.append(td2Body);
                    tableBody.append(td3Body);
                    tableBody.append(td4Body);
                    tableBody.append(td5Body);
                    tableBody.append(td6Body);
                    tbody.append(tableBody);

                  

                }

            } else {
                var h4data = document.createElement("h4");
                var childNode = document.createTextNode("No Data Available in Data base");
                h4data.appendChild(childNode);
                tbody.appendChild(h4data);
            }

            var body = document.getElementsByTagName('body')[0];
            body.appendChild(table);


        }

    }

    httpRequest.open("get", "http://localhost:3000/users", true);
    httpRequest.send();
}